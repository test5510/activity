<?php

namespace App\Method;

use App\DTO\ActivityFilter;
use App\Service\PageVisitService;
use Psr\Log\LoggerInterface;
use Timiki\Bundle\RpcServerBundle\Mapping as Rpc;

/**
 * @Rpc\Method("visitList")
 */
class VisitListMethod
{
    private PageVisitService $pageVisitService;
    private LoggerInterface  $logger;

    public function __construct(
        PageVisitService $pageVisitService,
        LoggerInterface $logger
    ) {
        $this->pageVisitService = $pageVisitService;
        $this->logger = $logger;
    }

    /**
     * @Rpc\Param()
     */
    protected ?string $sort = null;

    /**
     * @Rpc\Param()
     */
    protected ?int $limit = null;

    /**
     * @Rpc\Param()
     */
    protected ?int $offset = null;

    /**
     * @Rpc\Execute()
     */
    public function execute(): array
    {
        $this->logger->debug("VisitListMethod");
        $filter = new ActivityFilter($this->sort, $this->offset, $this->limit);

        return $this->pageVisitService->getPageVisitList($filter);
    }
}

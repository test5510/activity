<?php

namespace App\Method;

use App\DTO\PageVisitData;
use App\Service\PageVisitService;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Timiki\Bundle\RpcServerBundle\Mapping as Rpc;

/**
 * @Rpc\Method("visitLogger")
 */
class VisitLoggerMethod
{
    private PageVisitService $pageVisitService;
    private LoggerInterface  $logger;

    public function __construct(
        PageVisitService $pageVisitService,
        LoggerInterface $logger
    ) {
        $this->pageVisitService = $pageVisitService;
        $this->logger = $logger;
    }

    /**
     * @Rpc\Param()
     */
    protected ?string $url = null;

    /**
     * @Rpc\Param()
     */
    protected ?string $dateTime = null;

    /**
     * @Rpc\Execute()
     */
    public function execute()
    {
        $this->logger->debug("VisitLoggerMethod");
        $pageVisitData = new PageVisitData($this->url, Carbon::parse($this->dateTime));
        $this->pageVisitService->setPageVisit($pageVisitData);
    }
}

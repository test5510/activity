<?php

namespace App\Service;

use App\DTO\ActivityFilter;
use App\DTO\PageVisitData;
use App\Entity\PageVisit;
use App\Repository\PageVisitRepository;
use Doctrine\ORM\EntityManagerInterface;

class PageVisitService
{
    private PageVisitRepository    $pageVisitRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        PageVisitRepository $pageVisitRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->pageVisitRepository = $pageVisitRepository;
        $this->entityManager = $entityManager;
    }

    public function setPageVisit(PageVisitData $pageVisitData)
    {
        $pageVisit = new PageVisit($pageVisitData);
        $this->entityManager->persist($pageVisit);
        $this->entityManager->flush();
    }

    public function getPageVisitList(ActivityFilter $filter): array
    {
        return $this->pageVisitRepository->findAllWithFilters($filter);
    }
}

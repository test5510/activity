<?php

namespace App\Helpers;

class SortHelper
{
    /**
     * @param string $string
     *
     * @return array
     */
    public static function createSortArrayFromString(string $string): array
    {
        $result = [];

        $arraySort = explode(',', $string);
        foreach ($arraySort as $item) {
            $item = trim($item);
            if ($item) {
                if ($item[0] === '-' && strlen($item) > 1) {
                    $result[substr($item, 1)] = 'DESC';
                } else {
                    $result[$item] = 'ASC';
                }
            }
        }

        return $result;
    }
}

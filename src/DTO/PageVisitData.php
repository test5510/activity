<?php

namespace App\DTO;

use Carbon\Carbon;
use Symfony\Component\Validator\Constraints as Assert;

class PageVisitData
{
    /**
     * @Assert\Url()
     * @Assert\NotNull
     */
    public string $url;

    /**
     * @Assert\NotNull
     */
    public Carbon $dateTime;


    public function __construct(
        string $url,
        Carbon $dateTime
    ) {
        $this->url = $url;
        $this->dateTime = $dateTime;
    }

    public static function fromArray(array $params): self
    {
        return new self(
            $params['url'] ?? null,
            $params['dateTime'] ? Carbon::parse($params['dateTime']) : null,
        );
    }
}

<?php

namespace App\DTO;

class ActivityFilter
{
    public ?string $sort   = null;
    public ?int    $offset = null;
    public ?int    $limit  = null;

    public function __construct(
        ?string $sort,
        ?int $offset,
        ?int $limit
    ) {
        $this->sort = $sort;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    public static function fromArray(array $params): self
    {
        return new self(
            $params['sort'] ?? null,
            !empty($params['offset']) ? (int)$params['offset'] : null,
            !empty($params['limit']) ? (int)$params['limit'] : null
        );
    }

    public function toArray(): array
    {
        $output = [];
        if (null !== $this->sort) {
            $output['sort'] = $this->sort;
        }
        if (null !== $this->offset) {
            $output['offset'] = $this->offset;
        }
        if (null !== $this->limit) {
            $output['limit'] = $this->limit;
        }

        return $output;
    }
}

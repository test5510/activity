<?php

namespace App\Repository;

use App\DTO\ActivityFilter;
use App\Entity\PageVisit;
use App\Helpers\SortHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PageVisit|null find($id, $lockMode = null, $lockVersion = null)
 * @method PageVisit|null findOneBy(array $criteria, array $orderBy = null)
 * @method PageVisit[]    findAll()
 * @method PageVisit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageVisitRepository extends ServiceEntityRepository
{
    private const MAX_RESULT_COUNT = 50;

    /**
     * AppointmentSessionRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PageVisit::class);
    }

    /**
     * @param ActivityFilter|null $filter
     *
     * @return array
     */
    public function findAllWithFilters(ActivityFilter $filter = null): array
    {
        $qb = $this->createQueryBuilder('pv');
        $qb
            ->select('count(pv.url) as quantity, pv.url as url, max(pv.dateTime) as dateTime')
            ->groupBy('pv.url')
            ->orderBy('quantity','DESC')
            ->setMaxResults(static::MAX_RESULT_COUNT);

        if ($filter) {
            $qb = $this->applyFilters($qb, $filter);
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param              $filter
     *
     * @return QueryBuilder
     */
    private function applyFilters(QueryBuilder $qb, ActivityFilter $filter): QueryBuilder
    {
        if ($filter->offset) {
            $qb->setFirstResult($filter->offset);
        }

        if ($filter->limit && $filter->limit < self::MAX_RESULT_COUNT) {
            $qb->setMaxResults($filter->limit);
        }

        if ($filter->sort) {
            $sortList = SortHelper::createSortArrayFromString($filter->sort);
            foreach ($sortList as $field => $sort) {
                switch ($field) {
                    case 'dateTime':
                        $qb->addOrderBy('pv.dateTime', $sort);
                        break;
                    case 'url':
                        $qb->addOrderBy('pv.url', $sort);
                        break;
                    case 'id':
                        $qb->addOrderBy('pv.id', $sort);
                        break;
                }
            }
        }

        return $qb;
    }
}
